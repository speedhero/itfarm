package com.tc.itfarm.admin.action.article;


import com.google.common.collect.Lists;
import com.tc.itfarm.admin.biz.ArticleBiz;
import com.tc.itfarm.admin.biz.LoginBiz;
import com.tc.itfarm.api.common.Codes;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.CollectionUtil;
import com.tc.itfarm.api.util.StringUtil;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.Menu;
import com.tc.itfarm.service.ArticleService;
import com.tc.itfarm.service.CategoryService;
import com.tc.itfarm.service.MenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/article")
public class ArticleAdminAction {

	private static final Logger logger = LoggerFactory
			.getLogger(ArticleAdminAction.class);
	@Resource
	private ArticleService articleService;
	@Resource
	private CategoryService categoryService;
	@Resource
	private LoginBiz loginBiz;
	@Resource
	private ArticleBiz articleBiz;
	@Resource
	private MenuService menuService;

	/**
	 * 添加页面
	 * 
	 * @author: wangdongdong
	 * @date: 2016年7月15日
	 * @param model
	 * @return
	 */
	@RequestMapping("addArticleUI")
	public String addCategoryUI(Model model) {
		model.addAttribute("categorys", categoryService.selectAll());
		model.addAttribute("menus", menuService.selectAll());
		return "admin/article/addarticle";
	}
	
	/**
	 * 修改页面
	 * 
	 * @author: wangdongdong
	 * @date: 2016年7月15日
	 * @param model
	 * @return
	 */
	@RequestMapping("editArticleUI")
	public String editArticleUI(Model model,
			@RequestParam(value = "recordId", required = true) Integer recordId) {
		Article article = articleService.select(recordId);
		model.addAttribute("categorys", categoryService.selectAll());
		model.addAttribute("article", article);
		return "admin/article/addarticle";
	}

	/**
	 * 添加文章
	 * 
	 * @author: wangdongdong
	 * @date: 2016年7月15日
	 * @param content
	 * @param title
	 * @param typeId
	 * @return
	 */
	@RequestMapping(value = "addArticle", method = RequestMethod.POST)
	public String addCategory(
			@RequestParam(value = "content", defaultValue = "", required = true) String content,
			@RequestParam(value = "title") String title,
			@RequestParam(value = "typeId", required = true, defaultValue = "") Integer typeId,
			@RequestParam(value = "menuId", required = false, defaultValue = "") Integer menuId,
			@RequestParam(value = "titleImg", required = false) MultipartFile titleImg,
			HttpServletRequest request,
			@RequestParam(value = "keyword", defaultValue = "", required = true) String keyword,
			@RequestParam(value = "recordId", required = false) Integer recordId) {
		Article article = new Article();
		article.setTitle(title);
		article.setTypeId(typeId);
		article.setKeyword(keyword);
		article.setContent(content);
		if (recordId == null) {
			article.setAuthorId(loginBiz.getCurUser().getRecordId());
			article.setPageView(0);
			article.setTitleImg("001.png");
		} else {
			article.setRecordId(recordId);
		}
		if (titleImg != null && titleImg.getSize() != 0) {
			String path = request.getSession().getServletContext()
					.getRealPath("/titleImg/");
			String fileName = articleBiz.getImgFileName(titleImg
					.getOriginalFilename());
			File file = new File(path, fileName);
			if (!file.exists()) {
				file.mkdirs();
			}
			try {
				titleImg.transferTo(file);
			} catch (IllegalStateException | IOException e) {
				logger.error(e.getMessage());
			}
			article.setTitleImg(fileName);
		}
		articleService.save(article, menuId);
		return "redirect:articleList.do";
	}

	/**
	 * 文章列表
	 * 
	 * @author: wangdongdong
	 * @date: 2016年7月15日
	 * @param pageNo
	 * @param title
	 * @param typeId
	 * @return
	 */
	@RequestMapping("articleList")
	public ModelAndView articleList(
			@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(value = "title", required = false, defaultValue = "") String title,
			@RequestParam(value = "typeId", required = false) Integer typeId,
			HttpServletRequest request) {
		pageNo = pageNo == null ? 0 : pageNo;
		ModelAndView mv = new ModelAndView("admin/article/article_list");
		Page page = new Page(pageNo, Codes.COMMON_PAGE_SIZE);
		PageList<Article> pageList = articleService.selectArticleByPage(page,
				typeId, title);
		mv.addObject("articles", articleBiz.getArticleVOs(pageList.getData(), request));
		mv.addObject("page", pageList.getPage());
		mv.addObject("categorys", categoryService.selectAll());
		mv.addObject("title", title);
		mv.addObject("typeId", typeId);
		return mv;
	}

	@RequestMapping("/bacth")
	public String bacth(Integer [] id, String action, Integer new_cat_id) {
		if ("0".equals(action)) {
			return "redirect:articleList.do";
		} else {
			List<Integer> idList = CollectionUtil.arrayToList(id, Integer.class);
			if ("del_all".equals(action)) {
                articleService.delete(idList);
            } else {
                articleService.modifyToCategory(new_cat_id, idList);
            }
		}
		return "redirect:articleList.do";
	}

	@RequestMapping("/delete")
	public String delete(@RequestParam(value = "recordId", required = true) Integer recordId) {
		articleService.delete(recordId);
		return "redirect:articleList.do";
	}
}
