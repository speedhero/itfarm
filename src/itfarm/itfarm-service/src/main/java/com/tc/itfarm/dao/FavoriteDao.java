package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Favorite;
import com.tc.itfarm.model.FavoriteCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FavoriteDao extends SingleTableDao<Favorite, FavoriteCriteria> {
}