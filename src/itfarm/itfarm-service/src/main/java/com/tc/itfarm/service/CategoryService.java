package com.tc.itfarm.service;

import java.util.List;

import com.tc.itfarm.api.exception.BusinessException;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Category;

public interface CategoryService extends BaseService<Category> {
	/**
	 * 根据类型查找所有（0、其它 1、代码）
	 * @param type
	 * @return
	 */
	List<Category> selectAll(Short type);
	
	PageList<Category> selectByPage(Page page);
	
	void save(Category category) throws BusinessException;
	
}
