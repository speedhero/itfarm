package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Privilege implements Serializable {
    private Integer recordId;

    private Integer pid;

    private String privilegeCode;

    private String privilegeName;

    private String url;

    private String remark;

    private String iconCls;

    private Integer resourcetype;

    private Date createTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public Privilege(Integer recordId, Integer pid, String privilegeCode, String privilegeName, String url, String remark, String iconCls, Integer resourcetype, Date createTime, Date modifyTime) {
        this.recordId = recordId;
        this.pid = pid;
        this.privilegeCode = privilegeCode;
        this.privilegeName = privilegeName;
        this.url = url;
        this.remark = remark;
        this.iconCls = iconCls;
        this.resourcetype = resourcetype;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

    public Privilege() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode == null ? null : privilegeCode.trim();
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName == null ? null : privilegeName.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls == null ? null : iconCls.trim();
    }

    public Integer getResourcetype() {
        return resourcetype;
    }

    public void setResourcetype(Integer resourcetype) {
        this.resourcetype = resourcetype;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}